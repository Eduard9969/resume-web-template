'use strict';

// CONST
const gulp          = require('gulp');
const sass          = require('gulp-sass');
const pug           = require('gulp-pug');

const browserSync   = require('browser-sync').create();

const autoprefixer  = require('gulp-autoprefixer');

const imagemin      = require('gulp-imagemin');
const svgSprite     = require('gulp-svg-sprite');

const webpack       = require('webpack');
const webpackStream = require('webpack-stream');
const webpackConfig = require('./webpack.config.js');

// Js
gulp.task('js', function() {
    return gulp.src('./src/js/main.js')
        .pipe(webpackStream(webpackConfig), webpack)
        .pipe(gulp.dest('./build/dist/js'))
        .pipe(browserSync.stream());
});

// Sass
gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.sass')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer({cascade: false}))
        .pipe(gulp.dest('./build/dist/css'))
        .pipe(browserSync.stream());
});

// Pug
gulp.task('pug', function() {
    return gulp.src("./src/pug/*.pug")
        .pipe(pug())
        .pipe(gulp.dest("./build"))
        .pipe(browserSync.stream());
});

gulp.task('pug_decomposition', function() {
    return gulp.src("./src/pug/**/*.pug")
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest("./build/components"));
});

// Browser-Sync
function initBrowserSync() {
    return browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
}

// Compress Images
gulp.task('compress', function() {
    return gulp.src(['./src/img/**/*', '!./src/img/svg/**/*'])
        .pipe(imagemin())
        .pipe(gulp.dest('./build/dist/images'))
});

// Svg Sprite
gulp.task('svg_sprite', function () {
    return gulp.src('./src/img/svg/*.svg')
        .pipe(svgSprite({
                mode: {
                    stack: {
                        sprite: "../sprite.svg"
                    }
                },
            }
        ))
        .pipe(gulp.dest('./build/dist/images/svg'));
});

// Sass & Pug Watch
gulp.task('watch', function () {
    // Run Browser-Sync
    initBrowserSync();

    // Variables
    let img_execute = ['./src/img/**/*', '!./src/img/svg/**/*'];

    // Watcher Sass
    gulp.watch('./src/sass/**/*.sass', gulp.series('sass'));
    // Watcher Js
    gulp.watch('./src/js/**/*.js',     gulp.series('js'));
    // Watcher Pug
    gulp.watch('./src/pug/**/*.pug',   gulp.series(['pug_decomposition', 'pug']));
    // Watcher Img
    gulp.watch(img_execute,            gulp.series('compress'));
    // Watcher Svg
    gulp.watch('./src/img/svg/*.svg',  gulp.series('svg_sprite'));
});