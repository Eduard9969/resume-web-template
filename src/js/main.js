var app = function () {
    var module_tooltip = require('./modules/tooltip');

    module_tooltip.init('.tooltip-label > a');

    return {
        tooltip_c: module_tooltip
    }
}; 

module.exports = app();