var module_tooltip = function () {

    /**
     * tooltip init
     * @param tooltip_el
     */
    var tooltip_init = function(tooltip_el) {
        $(tooltip_el).on('click', function (e) {
            e.preventDefault();
            listen_tooltip(this)
        });

        control_area();
    };

    /**
     * tooltip toogle element
     * @type {*[]}
     */
    var tooltip_toogle = [];
    var listen_tooltip = function (tooltip_object) {
        var body    = $('body'),
            label   = $(tooltip_object).find('label'),
            ident   = label.attr('for');

        if (ident === undefined) return false;

        var element_ident = body.find('input#' + ident);
        if (element_ident.length === 0) return false;

        tooltip_toogle[ident] = !(tooltip_toogle[ident] !== undefined && tooltip_toogle[ident]);

        element_ident.prop('checked', tooltip_toogle[ident]);

        if (element_ident.prop('checked')) show_control_area();
        else show_control_area(false);
    };

    /**
     * control area listen
     */
    var control_area = function () {
        var area = $('body').find('.control-area');

        if (area.length > 0) {
            area.on('click', function (e) {
                e.preventDefault();

                control_area_all_hide();
                show_control_area(false);
            })
        }
    };

    /**
     * control area hidden all element
     */
    var control_area_all_hide = function () {
        $('*:checked').prop('checked', false);
        tooltip_toogle = [];
    }

    /**
     * constrol area toogle state
     * @param flag
     */
    var show_control_area = function (flag) {
        if (flag === undefined) flag = true;
        var area = $('body').find('.control-area');

        if (flag) area.show();
        else area.hide();
    };

    return {
        init:   tooltip_init
    }

}();

module.exports = module_tooltip;